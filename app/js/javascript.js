$(document).ready(function () {

  // On page load, class="is-active" is added to li element
  $('.u-ec-nav-menu__item--hover').filter(function () {
    return $(this).text() === 'Produkte';
  }).addClass('is-active');

  // Add active state on homepage navigation menu item when clicked.
  $('.u-ec-nav-menu__item--hover').click(function () {
    $('.is-active').removeClass('is-active');
    $(this).addClass('is-active');
  });

  // Navigation menu under hamburger menu icon

  $('.u-ec-menu').hide();

  $('.show-menu').click(function () {
    $('.u-ec-menu').slideToggle('fast');
  });

  $('.u-ec-menu__item--hover').click(function () {

    // highlight menu item in mobile view.

    /*$('.u-ec-menu__item--hover > .is-active').removeClass('is-active');
    $(this).addClass('is-active');*/

    $('.u-ec-menu').slideToggle('fast');
  });

  // Bread Crumb links
  var links = ['Home', 'Warme/ Kalte', 'Automatikfilter'];

  for (var i = 0; i < links.length; i++) {

    var item = $('<a class="u-ec-bread-crumb__item">').attr('href', '#').text(links[i]);

    item.appendTo('.u-ec-bread-crumb');

  }

  var product = {

    image: '../images/post-image.png',
    title: 'Boll Automatikfilter 6.18',
    subTitle: 'Geeignet fur hohen Druck - Fur stark belastete Medien',
    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    btn: '<div class="row"><div class="col-sm-12 col-xs-12"><a href="#" class="o-ec-product__btn pull-right">Mehr Erfahren</a></div></div>'

  };

  for (var a = 0; a < 1; a++) {

    $('<li class="o-ec-product__item">').appendTo('.o-ec-product');
    $('<div class="col-lg-3 col-md-4 col-sm-5 col-xs-12 pull-left o-ec-product__image o-ec-product__image-container"></div>').appendTo('.o-ec-product__item');
    $('.o-ec-product__image').append($('<img />').attr('src', product.image));
    $('<div class="col-lg-9 col-md-8 col-sm-7 hidden-xs pull-right o-ec-product__content"></div>').insertAfter('.o-ec-product__image');

    $('.o-ec-product__content').append($('<h1 class="o-ec-product__heading">').text(product.title));
    $('.o-ec-product__content').append($('<h4 class="o-ec-product__heading">').text(product.subTitle));
    $('.o-ec-product__content').append($('<div class="o-ec-product__info">').text(product.text));
    $('.o-ec-product__content').append(product.btn);

    if ($(window).width() < 767) {
      $('<h1 class="o-ec-product__heading">').text(product.title).appendTo('.o-ec-product__image-container');
      $('<h4 class="o-ec-product__heading">').text(product.subTitle).appendTo('.o-ec-product__image-container');
      $('<div class="o-ec-product__info">').text(product.text).appendTo('.o-ec-product__image-container');
      $('.o-ec-product__image-container').append(product.btn);
    }

  }

  /*var listItem = $('<li class="o-ec-product__item">');
  var itemImageContainer = $('<div class="col-lg-3 col-md-4 col-sm-5 col-xs-12 pull-left o-ec-product__image o-ec-product__image-container"></div>');
  var productInfoWrapper = $('<div class="col-lg-9 col-md-8 col-sm-7 hidden-xs pull-right"></div>');

  if ($(window).width() > 767) {
    $('.o-ec-product__image').append($('<img />').attr('src', product.image));
    $('.o-ec-product__title').append($('<h1 class="o-ec-product__heading">').text(product.title));
    $('.o-ec-product__subtitle').append($('<h4 class="o-ec-product__heading">').text(product.subTitle));
    $('.o-ec-product__text').append($('<div class="o-ec-product__info">').text(product.text));
    $('.o-ec-btn-container').append(product.btn);
  }

  for (var a = 0; a < 1; a++) {

    listItem.appendTo('.o-ec-product'); // ul.
    itemImageContainer.appendTo(listItem); // wrapper for product image in list item.
    productInfoWrapper.insertAfter('.o-ec-product__image-container');

    $(window).resize(function () {

      if ($(window).width() <= 767) {

        $('.o-ec-product__image-container').append($('<img />').attr('src', product.image));
        $('.o-ec-product__image-container').append($('<h1 class="o-ec-product__heading">').text(product.title));
        $('.o-ec-product__image-container').append($('<h4 class="o-ec-product__heading">').text(product.subTitle));
        $('.o-ec-product__image-container').append($('<div class="o-ec-product__info">').text(product.text));
        $('.o-ec-product__image-container').append(product.btn);

      }

    });

  }*/

});